

// the setup routine runs once when you press reset:
void setup() {
  //Set pin 6 pwm frequency to 7812.5Hz (31250/4)
  //WARNING: This may break timers, etc!
  //Eg, delay() won't work right.
  setPwmFrequency(6, 4);
  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
}

// the loop routine runs over and over again forever:
void loop() {
  int potValue = analogRead(A2);
  delay(5);        // delay in between reads for stability
  
  int buckOutput = analogRead(A0);
  
  Serial.print("Pot setting:");
  Serial.println(potValue);
  
  Serial.print("Output:");
  Serial.println(buckOutput);
  delay(1000);
  if(potValue > 1000){
    pinMode(3,OUTPUT); //Drive to drive the transistor on harder via a smaller base resistor than #6
    digitalWrite(3,0);
  }else{
    pinMode(3,INPUT); //High-z, turning the 820R base resistor off.
  }
  analogWrite(6,255 - potValue/8);
}

void setPwmFrequency(int pin, int divisor) {
  byte mode;
  if(pin == 5 || pin == 6 || pin == 9 || pin == 10) {
    switch(divisor) {
      case 1: mode = 0x01; break;
      case 8: mode = 0x02; break;
      case 64: mode = 0x03; break;
      case 256: mode = 0x04; break;
      case 1024: mode = 0x05; break;
      default: return;
    }
    if(pin == 5 || pin == 6) {
      TCCR0B = TCCR0B & 0b11111000 | mode;
    } else {
      TCCR1B = TCCR1B & 0b11111000 | mode;
    }
  } else if(pin == 3 || pin == 11) {
    switch(divisor) {
      case 1: mode = 0x01; break;
      case 8: mode = 0x02; break;
      case 32: mode = 0x03; break;
      case 64: mode = 0x04; break;
      case 128: mode = 0x05; break;
      case 256: mode = 0x06; break;
      case 1024: mode = 0x07; break;
      default: return;
    }
    TCCR2B = TCCR2B & 0b11111000 | mode;
  }
}
